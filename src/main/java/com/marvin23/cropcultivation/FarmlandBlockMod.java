package com.marvin23.cropcultivation;

import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public final class FarmlandBlockMod extends Block {

    public static final IntProperty moistureLevel = IntProperty.of(Constants.Properties.MOISTURE_LEVEL,0,5);

    public FarmlandBlockMod() {
        super(AbstractBlock.Settings.create()
                .mapColor(MapColor.DIRT_BROWN)
                .ticksRandomly()
                .strength(0.6f)
                .sounds(BlockSoundGroup.GRAVEL)
                .blockVision(Blocks::always)
                .suffocates(Blocks::always));
        setDefaultState(getDefaultState().with(moistureLevel, 4));
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return Block.createCuboidShape(0.0, 0.0, 0.0, 16.0, 15.0, 16.0);
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(moistureLevel);
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        player.playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1, 1);
        player.sendMessage(Text.of(world.getBlockState(pos).toString()));

        if(player.inPowderSnow) {
            world.setBlockState(pos, state.with(moistureLevel, 5));
            player.sendMessage(Text.of(world.getBlockState(pos).toString()));
        }
        return ActionResult.SUCCESS;
    }
}
