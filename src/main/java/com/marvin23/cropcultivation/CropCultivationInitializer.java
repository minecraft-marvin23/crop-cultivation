package com.marvin23.cropcultivation;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.MutableRegistry;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public final class CropCultivationInitializer implements ModInitializer {
	@Override
	public void onInitialize() {
		Registry.register(Registries.BLOCK, new Identifier(Constants.MOD_ID, Constants.Blocks.FARMLAND_BLOCK_MOD), BlockRegistry.FARMLAND_BLOCK_MOD);
		Registry.register(Registries.ITEM, new Identifier(Constants.MOD_ID, Constants.Blocks.FARMLAND_BLOCK_MOD), new BlockItem(BlockRegistry.FARMLAND_BLOCK_MOD, new Item.Settings()));
	}
}