package com.marvin23.cropcultivation;

public final class Constants {
    public static final String MOD_ID = "cropcultivation";

    public static class Properties {
        public static final String MOISTURE_LEVEL = "moisture_level";
    }
    public static class Blocks {
        public static final String FARMLAND_BLOCK_MOD = "farmland_block_mod";
   }
}
