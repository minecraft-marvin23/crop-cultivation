package com.marvin23.cropcultivation;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;

// Fabric cannot instantiate the blocks directly in init for whatever reason
public final class BlockRegistry {
    public static final Block FARMLAND_BLOCK_MOD = new FarmlandBlockMod();
}
